<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP ACTIVITY 5</title>
</head>
<body>

	<?php session_start(); ?>
	<h2>Login</h2>

	<form method="POST" action="./server.php">
		Email: <input type="text" name="email" required> 
		Password: <input type="text" name="password" required>
		<button type="submit">login</button>
	</form>

	<?php if(isset($_SESSION['email'])): ?>
			<p>Hello, <?= $_SESSION['email']; ?></p>

	<?php else: ?>
		<?php if(isset($_SESSION['login_error'])): ?>

			<p><?= $_SESSION['login_error']; ?></p>

		<?php endif;?>
	<?php endif;?>



</body>
</html>